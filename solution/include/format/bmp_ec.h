#pragma once

#define ERROR_CODES(X)                                                         \
  X(bmp_EINVALID_HEADER, "failed to parse the bmp header")                     \
  X(bmp_EUNSUPPORTED_HEADER,                                                   \
    "only the BITMAPINFOHEADER DIB header is supported")                       \
  X(bmp_EUNSUPPORTED_MARKER,                                                   \
    "Only BM is supported as a marker in the bmp header")                      \
  X(bmp_ENEGATIVE_DIMENSIONS, "Negative width or height are not supported")    \
  X(bmp_EINVALID_N_PLANES, "The number of color planes must be 1")             \
  X(bmp_EINVALID_OFFSET, "The offset of image data overlaps with headers")     \
  X(bmp_EOFFSET_TOO_BIG, "The offset of image data is too big")                \
  X(bmp_ESEEK_FAILED, "Failed seeking the bmp file")                           \
  X(bmp_ETELL_FAILED, "Failed telling the bmp file")                           \
  X(bmp_EREAD_FAILED, "Failed reading the bmp file")                           \
  X(bmp_EUNSUPPORTED_COMPRESSION, "Only BI_RGB compression is supported")      \
  X(bmp_EUNSUPPORTED_COLOR_DEPTH, "Only 24-bit color depth is supported")      \
  X(bmp_EINCORRECT_IMG_SIZE,                                                   \
    "Calculated image size differs from the one specified in the header")      \
  X(bmp_EIMG_TOO_BIG, "The image is too big to print")                         \
  X(bmp_EWRITE_FAILED, "Failed writing the bmp file")

#define ERROR_ENUM(NAME, TEXT) NAME,
#define ERROR_TEXT(NAME, TEXT)                                                 \
  case NAME:                                                                   \
    return TEXT;

typedef enum { bmp_OK = 0, ERROR_CODES(ERROR_ENUM) } bmp_ec;

static inline const char *bmp_strerror(bmp_ec ec) {
  switch (ec) {
    ERROR_CODES(ERROR_TEXT)
  case bmp_OK:
    return "Everything's fine really";
  default:
    return "Unknown error";
  }
}

#undef ERROR_CODES
#undef ERROR_ENUM
#undef ERROR_TEXT
