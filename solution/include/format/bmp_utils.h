#include <stddef.h> // size_t
#include <stdint.h> // uint16_t, uint32_t, int32_t

#define BMP_MARKER 0x4D42
#define BMP_IMPL_COLOR_DEPTH 24
#define BMP_HEADER_SIZE 14 // in bytes
#define DIB_HEADER_SIZE 40 // in bytes

struct bmp_header {
  uint16_t marker;
  uint32_t file_size;
  uint32_t reserved;
  uint32_t raw_img_offset;
};

struct dib_header {
  uint32_t header_size;
  int32_t width;
  int32_t height;
  uint16_t n_planes;
  uint16_t color_depth;
  uint32_t compression;
  uint32_t raw_img_size;
  int32_t horiz_resolution; // pixel per metre
  int32_t vert_resolution;  // pixel per metre
  uint32_t n_colors;
  uint32_t n_important_colors;
};

static inline size_t calculate_row_size(size_t width, uint16_t color_depth) {
  return (width * color_depth + 31) / 32 * 4;
}

static inline size_t calculate_img_size(size_t width, size_t height,
                                        uint16_t color_depth) {
  return calculate_row_size(width, color_depth) * height;
}

static inline size_t impl_calculate_row_size(size_t width) {
  return calculate_row_size(width, BMP_IMPL_COLOR_DEPTH);
}

static inline size_t impl_calculate_img_size(size_t width, size_t height) {
  return calculate_img_size(width, height, BMP_IMPL_COLOR_DEPTH);
}
