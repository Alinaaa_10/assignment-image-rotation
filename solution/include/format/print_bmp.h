#pragma once

#include "format/bmp_ec.h" // bmp_ec
#include "image.h"         // image

#include <stdio.h> // FILE

bmp_ec print_bmp(FILE *file, struct image img);
