#pragma once

#include <stddef.h> // size_t
#include <stdint.h> // uint8_t
#include <stdlib.h> // free

struct __attribute__((packed)) pixel {
  uint8_t b, g, r;
};

struct image {
  size_t width, height;
  struct pixel *data;
};

static inline void image_destroy(struct image image) { free(image.data); }
