#pragma once

#include <stdio.h>  // fprintf
#include <stdlib.h> // size_t, malloc, exit, EXIT_FAILURE

static inline void *xmalloc(size_t size) {
  void *res = malloc(size);
  if (!res) {
    fprintf(stderr, "malloc(%zu) failed", size);
    exit(EXIT_FAILURE);
  }
  return res;
}

#define ec_checked(ec, f, on_ec)                                               \
  do {                                                                         \
    (ec) = (f);                                                                \
    if (ec) {                                                                  \
      return (on_ec);                                                          \
    }                                                                          \
  } while (0)
