#include "format/bmp.h" // bmp_ec
#include "format/bmp_utils.h" // bmp_header, dib_header, calculated_img_size, calculate_row_size
#include "util.h" // xmalloc

#include <assert.h> // assert
#include <stdio.h>  // fread, fseek, SEEK_SET

_Static_assert(sizeof(uint32_t) == 4,
               "'char' is not 8 bits wide, don't know how to work with that.");

static inline bmp_ec check_compression(uint32_t compression,
                                       uint16_t color_depth) {
  if (compression != 0) {
    return bmp_EUNSUPPORTED_COMPRESSION;
  }
  if (color_depth != BMP_IMPL_COLOR_DEPTH) { // for BI_RGB
    return bmp_EUNSUPPORTED_COLOR_DEPTH;
  }
  return bmp_OK;
}

static bmp_ec parse_uint32(FILE *file, uint32_t *res) {
  unsigned char buf[4];
  size_t nread = fread(buf, sizeof(char), 4, file);
  if (nread != 4) {
    return bmp_EREAD_FAILED;
  }
  *res = buf[0] + buf[1] * 0x100 + buf[2] * 0x10000 + buf[3] * 0x1000000;
  return bmp_OK;
}

static bmp_ec parse_uint16(FILE *file, uint16_t *res) {
  unsigned char buf[2];
  size_t nread = fread(buf, sizeof(char), 2, file);
  if (nread != 2) {
    return bmp_EREAD_FAILED;
  }
  *res = buf[0] + buf[1] * 0x100;
  return bmp_OK;
}

static bmp_ec parse_bmp_header(FILE *file, struct bmp_header *bmp_header) {
  bmp_ec ec;
  struct bmp_header *h = bmp_header;
  ec_checked(ec, parse_uint16(file, &h->marker), bmp_EINVALID_HEADER);
  if (h->marker != BMP_MARKER) {
    return bmp_EUNSUPPORTED_MARKER;
  }
  ec_checked(ec, parse_uint32(file, &h->file_size), bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, &h->reserved), bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, &h->raw_img_offset), bmp_EINVALID_HEADER);
  return bmp_OK;
}

static bmp_ec parse_dib_header(FILE *file, struct dib_header *dib_header) {
  bmp_ec ec;
  struct dib_header *h = dib_header;
  ec_checked(ec, parse_uint32(file, &h->header_size), bmp_EINVALID_HEADER);
  if (h->header_size != 40) {
    return bmp_EUNSUPPORTED_HEADER;
  }
  ec_checked(ec, parse_uint32(file, (uint32_t *)&h->width),
             bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, (uint32_t *)&h->height),
             bmp_EINVALID_HEADER);
  if (h->width < 0 || h->height < 0) {
    return bmp_ENEGATIVE_DIMENSIONS;
  }
  ec_checked(ec, parse_uint16(file, &h->n_planes), bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint16(file, &h->color_depth), bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, &h->compression), bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, &h->raw_img_size), bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, (uint32_t *)&h->horiz_resolution),
             bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, (uint32_t *)&h->vert_resolution),
             bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, &h->n_colors), bmp_EINVALID_HEADER);
  ec_checked(ec, parse_uint32(file, &h->n_important_colors),
             bmp_EINVALID_HEADER);
  if (h->n_planes != 1) {
    return bmp_EINVALID_N_PLANES;
  }
  ec_checked(ec, check_compression(h->compression, h->color_depth), ec);
  // почему-то в тестах размер некорректный, на 2 байта больше
  /* size_t calculated_img_size = */
  /*     calculate_img_size((size_t)h->width, (size_t)h->height,
   * h->color_depth); */
  /* if (h->raw_img_size && h->raw_img_size != calculated_img_size) { */
  /*   return bmp_EINCORRECT_IMG_SIZE; */
  /* } */
  return bmp_OK;
}

static void parse_pixels(unsigned char const *data, size_t width, size_t height,
                         struct pixel *pixels) {
  size_t pos = 0;
  size_t row_size = impl_calculate_row_size(width);
  size_t padding = row_size - width * 3;
  for (size_t i = 0; i < height; ++i) {
    for (size_t j = 0; j < width; ++j) {
      uint8_t b = data[pos++];
      uint8_t g = data[pos++];
      uint8_t r = data[pos++];
      pixels[i * width + j] = (struct pixel){.b = b, .g = g, .r = r};
    }
    pos += padding;
  }
}

bmp_ec parse_bmp(FILE *file, struct image *res) {
  bmp_ec ec;
  struct bmp_header bmp_header;
  struct dib_header dib_header;
  ec_checked(ec, parse_bmp_header(file, &bmp_header), ec);
  ec_checked(ec, parse_dib_header(file, &dib_header), ec);
  int32_t width = dib_header.width, height = dib_header.height;

  long nread_so_far = ftell(file);
  if (nread_so_far == -1) {
    return bmp_ETELL_FAILED;
  }
  assert(nread_so_far == BMP_HEADER_SIZE + DIB_HEADER_SIZE);
  if (bmp_header.raw_img_offset < (uint32_t)nread_so_far) {
    return bmp_EINVALID_OFFSET;
  }
  if (fseek(file, bmp_header.raw_img_offset, SEEK_SET)) {
    return bmp_ESEEK_FAILED;
  }

  size_t calculated_img_size =
      calculate_img_size((size_t)width, (size_t)height, dib_header.color_depth);
  unsigned char *img = xmalloc(calculated_img_size);
  size_t nread = fread(img, 1, calculated_img_size, file);
  if (nread != calculated_img_size) {
    free(img);
    return bmp_EREAD_FAILED;
  }
  size_t n_pixels = (size_t)width * (size_t)height;
  struct pixel *pixels = xmalloc(sizeof(struct pixel) * n_pixels);
  parse_pixels(img, (size_t)width, (size_t)height, pixels);
  free(img);
  res->width = (uint64_t)width;
  res->height = (uint64_t)height;
  res->data = pixels;
  return bmp_OK;
}
