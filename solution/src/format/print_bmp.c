#include "format/print_bmp.h" // bmp_ec
#include "format/bmp_utils.h" // bmp_header, dib_header, calculate_row_size, calculate_img_size
#include "util.h" // ec_set, xmalloc

_Static_assert(sizeof(uint32_t) == 4,
               "'char' is not 8 bits wide, don't know how to work with that.");

static bmp_ec print_uint32(FILE *file, uint32_t x) {
  unsigned char buf[4] = {
      x & 0xFF,
      (x >> 8) & 0xFF,
      (x >> 16) & 0xFF,
      (x >> 24) & 0xFF,
  };
  size_t nwrit = fwrite(buf, 1, 4, file);
  if (nwrit != 4) {
    return bmp_EWRITE_FAILED;
  }
  return bmp_OK;
}

static bmp_ec print_uint16(FILE *file, uint16_t x) {
  unsigned char buf[2] = {x & 0xFF, (x >> 8) & 0xFF};
  size_t nwrit = fwrite(buf, 1, 2, file);
  if (nwrit != 2) {
    return bmp_EWRITE_FAILED;
  }
  return bmp_OK;
}

static bmp_ec print_bmp_header(FILE *file, struct bmp_header bmp_header) {
  bmp_ec ec;
  ec_checked(ec, print_uint16(file, bmp_header.marker), ec);
  ec_checked(ec, print_uint32(file, bmp_header.file_size), ec);
  ec_checked(ec, print_uint32(file, bmp_header.reserved), ec);
  ec_checked(ec, print_uint32(file, bmp_header.raw_img_offset), ec);
  return bmp_OK;
}

static bmp_ec print_dib_header(FILE *file, struct dib_header dib_header) {
  bmp_ec ec;
  ec_checked(ec, print_uint32(file, dib_header.header_size), ec);
  ec_checked(ec, print_uint32(file, (uint32_t)dib_header.width), ec);
  ec_checked(ec, print_uint32(file, (uint32_t)dib_header.height), ec);
  ec_checked(ec, print_uint16(file, dib_header.n_planes), ec);
  ec_checked(ec, print_uint16(file, dib_header.color_depth), ec);
  ec_checked(ec, print_uint32(file, dib_header.compression), ec);
  ec_checked(ec, print_uint32(file, dib_header.raw_img_size), ec);
  ec_checked(ec, print_uint32(file, (uint32_t)dib_header.horiz_resolution), ec);
  ec_checked(ec, print_uint32(file, (uint32_t)dib_header.vert_resolution), ec);
  ec_checked(ec, print_uint32(file, dib_header.n_colors), ec);
  ec_checked(ec, print_uint32(file, dib_header.n_important_colors), ec);
  return bmp_OK;
}

void print_pixels(struct pixel const *pixels, size_t width, size_t height,
                  unsigned char *buf) {
  size_t pos = 0;
  size_t row_size = impl_calculate_row_size(width);
  size_t padding = row_size - width * 3;
  for (size_t i = 0; i < height; ++i) {
    for (size_t j = 0; j < width; ++j) {
      buf[pos++] = pixels[i * width + j].b;
      buf[pos++] = pixels[i * width + j].g;
      buf[pos++] = pixels[i * width + j].r;
    }
    for (size_t k = 0; k < padding; ++k) {
      buf[pos++] = 0;
    }
  }
}

struct bmp_header mk_bmp_header(size_t width, size_t height) {
  size_t img_size = impl_calculate_img_size(width, height);
  return (struct bmp_header){
      .marker = BMP_MARKER,
      .file_size = (uint32_t)img_size + BMP_HEADER_SIZE + DIB_HEADER_SIZE,
      .reserved = 0,
      .raw_img_offset = BMP_HEADER_SIZE + DIB_HEADER_SIZE,
  };
}

struct dib_header mk_dib_header(size_t width, size_t height) {
  size_t img_size = impl_calculate_img_size(width, height);
  return (struct dib_header){
      .header_size = (uint32_t)DIB_HEADER_SIZE,
      .width = (int32_t)width,
      .height = (int32_t)height,
      .n_planes = 1,
      .color_depth = BMP_IMPL_COLOR_DEPTH,
      .compression = 0,
      .raw_img_size = (uint32_t)img_size,
      .horiz_resolution = 0,
      .vert_resolution = 0,
      .n_colors = 0,
      .n_important_colors = 0,
  };
}

bmp_ec print_bmp(FILE *file, struct image img) {
  if (img.width > INT32_MAX || img.height > INT32_MAX) {
    return bmp_EIMG_TOO_BIG;
  }
  struct bmp_header bmp_header = mk_bmp_header(img.width, img.height);
  struct dib_header dib_header = mk_dib_header(img.width, img.height);
  bmp_ec ec;
  ec_checked(ec, print_bmp_header(file, bmp_header), ec);
  ec_checked(ec, print_dib_header(file, dib_header), ec);
  size_t img_size = impl_calculate_img_size(img.width, img.height);
  unsigned char *buf = xmalloc(img_size);
  print_pixels(img.data, img.width, img.height, buf);
  size_t nwrit = fwrite(buf, 1, img_size, file);
  free(buf);
  if (nwrit != img_size) {
    return bmp_EWRITE_FAILED;
  }
  return bmp_OK;
}
