#include "format/bmp.h"
#include "manip/rotate.h"

#include <assert.h> // assert
#include <stdio.h>  // fprintf, stderr
#include <stdlib.h> // exit, EXIT_FAILURE

static void print_usage(char const *exec_name) {
  fprintf(stderr, "USAGE: %s <source-image> <transformed-image>", exec_name);
}

int main(int argc, char **argv) {
  if (argc != 3) {
    print_usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  bmp_ec ec;
  char const *source_image_filepath = argv[1];
  FILE *src = fopen(source_image_filepath, "rb");
  if (!src) {
    perror("Failed to open the source file");
    return EXIT_FAILURE;
  }
  struct image image;
  if ((ec = parse_bmp(src, &image))) {
    if (ferror(src)) {
      perror("IO error");
    }
    fprintf(stderr, "%s", bmp_strerror(ec));
    if (fclose(src)) {
      perror("IO error");
    }
    return EXIT_FAILURE;
  }
  if (fclose(src)) {
    perror("IO error");
  }

  struct image rotated = rotate(image);
  image_destroy(image);

  char const *target_image_filepath = argv[2];
  FILE *dest = fopen(target_image_filepath, "wb");
  if (!dest) {
    perror("Failed to open the target file");
    image_destroy(rotated);
    return EXIT_FAILURE;
  }
  if ((ec = print_bmp(dest, rotated))) {
    if (ferror(dest)) {
      perror("IO error");
    }
    fprintf(stderr, "%s", bmp_strerror(ec));
    fclose(dest);
    image_destroy(rotated);
    return EXIT_FAILURE;
  }
  if (fclose(dest)) {
    perror("IO error");
  }
  image_destroy(rotated);

  return 0;
}
