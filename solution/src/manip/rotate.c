#include "manip/rotate.h" // rotate, image, pixel
#include "util.h"         // xmalloc

#include <malloc.h> // malloc
#include <stddef.h> // NULL, size_t

struct image rotate(struct image source) {
  size_t width = source.height;
  size_t height = source.width;
  struct pixel *data = xmalloc(sizeof(struct pixel) * height * width);
  for (size_t i = 0; i < height; ++i) {
    for (size_t j = 0; j < width; ++j) {
      data[width * i + j] =
          source.data[(source.height - 1 - j) * source.width + i];
      // [0, 1]
      // [2, 3] <= [4, 2, 0]
      // [4, 5]    [5, 3, 1]
    }
  }
  return (struct image){.width = width, .height = height, .data = data};
}
